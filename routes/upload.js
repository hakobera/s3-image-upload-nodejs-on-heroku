var AWS = require('aws-sdk'),
    fs = require('fs'),
    util = require('util'),
    async = require('async'),
    im = require('imagemagick');

AWS.config.update({
  accessKeyId: process.env.AWS_ACCESS_KEY_ID,
  secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY,
  region: process.env.AWS_REGION
});

var s3 = new AWS.S3();
var bucket = process.env.S3_BUCKET;

exports.post = function (req, res) {
  var uploadFile = req.files.upload;
  if (!uploadFile) {
    return res.send('No file is uploaded!');
  }

  var bucket = process.env.S3_BUCKET,
      key = uploadFile.name;

  async.waterfall([
    function (next) {
      var srcPath = uploadFile.path;
      var dstPath = srcPath + '_converted';

      console.log('src=%s, dst=%s', srcPath, dstPath);

      im.resize({
        srcPath: srcPath,
        dstPath: dstPath,
        width: 128,
        customArgs: [ '-auto-orient' ]
      }, function (err, stdout, stderr) {
        if (err) return next(err);
        fs.readFile(dstPath, next);
      });
    },
    function (data, next) {
      var obj = {
        Bucket: bucket,
        ACL: 'public-read',
        Key: key,
        Body: data
      };
      s3.client.putObject(obj, next);
    }
  ], function (err, result) {
    if (err) {
      return res.send(500, err.message);
    }

    var imageUrl = util.format('http://%s.s3.amazonaws.com/%s', bucket, key);
    console.log(imageUrl);
    res.render('result', { title: 'result', imageUrl: imageUrl });
  });
};
