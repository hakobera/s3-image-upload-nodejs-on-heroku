# s3-image-upload-nodejs-on-heroku

## How to deploy on Heroku

```sh
$ git clone git@bitbucket.org:hakobera/s3-image-upload-nodejs-on-heroku.git
$ cd s3-image-upload-nodejs-on-heroku
$ heroku create
$ heroku config:add AWS_ACCESS_KEY_ID=<access-key>
$ heroku config:add AWS_SECRET_ACCESS_KEY=<secret-key>
$ heroku config:add AWS_REGION=<aws-region-name>
$ heroku config:add S3_BUCKET=<s3-bucket-name>
$ git push heroku master
```

After deploy, type following command, then open the app on your browser.

```sh
$ heroku open
```
